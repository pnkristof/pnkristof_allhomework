﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pn_kristof_rdi2.RogueGame.Jatek.Jatekter;
using pn_kristof_rdi2.RogueGame.Jatek.Megjelenites;

namespace pn_kristof_rdi2.RogueGame.Jatek.Szabalyok
{
    delegate void KincsFelvetelKezelo(Kincs kincs, Jatekos jatekos);
    delegate void JatekosValtozasKezelo(Jatekos jatekos, int ujPont, int ujElet);

    class Fal : RogzitettJatekElem, IKirajzolhato
    {
        public override double Meret
        {
            get { return 1; }
        }
        public char Alak
        {
            get { return '\u2593'; }
        }

        public Fal(int x, int y, JatekTer ter) : base(x, y, ter)
        { }

        public override void Utkozes(JatekElem elem)
        { }

        
    }
}

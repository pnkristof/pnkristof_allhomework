﻿using pn_kristof_rdi2.RogueGame.Jatek.Automatizmus;
using pn_kristof_rdi2.RogueGame.Jatek.Jatekter;
using pn_kristof_rdi2.RogueGame.Jatek.Szabalyok;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pn_kristof_rdi2
{
    class GepiJatekos : Jatekos, IAutomatikusanMukodo
    {
        static Random RandomGenerator = new Random();

        public override char Alak
        {
            get { return '\u2640'; }
        }
        public int MukodesIntervallum
        { get { return 2; } }

        public GepiJatekos(string nev, int x, int y, JatekTer ter) : base(nev, x, y, ter)
        { }

        public void Mozgas()
        {
            int rng = RandomGenerator.Next(0, 4);
            switch (rng)                                
            {
                case 0:
                    Megy(0, -1);
                    break;
                case 1:
                    Megy(0, 1);
                    break;
                case 2:
                    Megy(-1, 0);
                    break;
                case 3:
                    Megy(1, 0);
                    break;
                default:
                    break;
            }
        }
        public void Mukodik()
        {
            Mozgas();
        }
    }
}

﻿using pn_kristof_rdi2.RogueGame.Jatek.Jatekter;
using pn_kristof_rdi2.RogueGame.Jatek.Szabalyok;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pn_kristof_rdi2
{
    class GonoszGepiJatekos : GepiJatekos
    {
        public override char Alak
        {
            get { return '\u2642'; }
        }

        public GonoszGepiJatekos(string nev, int x, int y, JatekTer ter) : base(nev, x, y, ter)
        { }

        public override void Utkozes(JatekElem elem)
        {
            base.Utkozes(elem);
            if (elem is Jatekos && this.Aktiv)
                (elem as Jatekos).Serul(10);
        }
    }
}

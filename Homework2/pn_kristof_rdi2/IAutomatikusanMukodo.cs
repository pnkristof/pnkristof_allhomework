﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pn_kristof_rdi2.RogueGame.Jatek.Automatizmus

{
    interface IAutomatikusanMukodo
    {
        void Mukodik();
        int MukodesIntervallum  { get; }
    }
}

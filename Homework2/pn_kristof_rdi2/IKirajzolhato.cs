﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pn_kristof_rdi2.RogueGame.Jatek.Megjelenites
{
    interface IKirajzolhato
    {
        int X { get; }
        int Y { get; }

        char Alak { get; }

        
    }
}

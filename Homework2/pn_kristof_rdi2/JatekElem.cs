﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pn_kristof_rdi2.RogueGame.Jatek.Jatekter
{
    abstract class JatekElem
    {

        private int x;
        private int y;

        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }

        protected JatekTer ter;

        virtual public double Meret { get; private set; }
        
        public abstract void Utkozes(JatekElem elem);

        public JatekElem(int x, int y, JatekTer ter)
        {
            this.x = x;
            this.y = y;
            this.ter = ter;
            ter.Felvetel(this);
        }
    }
}

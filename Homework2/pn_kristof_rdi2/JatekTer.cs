﻿using pn_kristof_rdi2.RogueGame.Jatek.Megjelenites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pn_kristof_rdi2.RogueGame.Jatek.Jatekter
{
    class JatekTer : IMegjelenitheto
    {
        const int MAX_ELEMSZAM = 1000;
        private int elemN;
        private JatekElem[] elemek;

        private int meretX;
        private int meretY;

        public int MeretX { get => meretX; private set => meretX = value; }
        public int MeretY { get => meretY; private set => meretY = value; }

        public int[] MegjelenitendoMeret
        {
            get
            {
                return new int[] { meretX, meretY };
            }
        }

        public JatekTer(int meretX, int meretY)
        {
            this.meretX = meretX;
            this.meretY = meretY;

            elemek = new JatekElem[MAX_ELEMSZAM];
            elemN = 0;
        }

        public void Felvetel(JatekElem elem)
        {
            elemek[elemN++] = elem;
        }
        public void Torles(JatekElem elem)
        {
            for (int i = 0; i < elemN; i++)
            {
                if (elemek[i].Equals(elem))
                {
                    elemek[i] = elemek[--elemN];
                    elemek[elemN] = null;
                    return;
                }
            }
        }

        public JatekElem[] MegadottHelyenLevok(int x, int y, int tavolsag)
        {
            int mhlevok = 0;
            for (int i = 0; i < elemN; i++)
            {
                if (((elemek[i].X - x) * (elemek[i].X - x) + (elemek[i].Y - y) * (elemek[i].Y - y)) <= tavolsag * tavolsag)
                    mhlevok++;
            }
            var megfelelElemek = new JatekElem[mhlevok];
            for (int i = 0; i < elemN; i++)
                if (((elemek[i].X - x) * (elemek[i].X - x) + (elemek[i].Y - y) * (elemek[i].Y - y)) <= tavolsag * tavolsag)
                    megfelelElemek[--mhlevok] = elemek[i];
            return megfelelElemek;
        }

        public JatekElem[] MegadottHelyenLevok(int x, int y)
        {
            return MegadottHelyenLevok(x, y, 0);
        }

        public IKirajzolhato[] MegjelenitendoElemek()
        {
            int count = 0;
            for (int i = 0; i < elemN; i++)
            {

                if (elemek[i] is IKirajzolhato)
                {
                    count++;
                }
            }
            var megjelenithetok = new IKirajzolhato[count];

            for (int i = 0; i < elemN; i++)
            {
                if (elemek[i] is IKirajzolhato)
                {
                    megjelenithetok[--count] = elemek[i] as IKirajzolhato;
                }
            }
            return megjelenithetok;

        }
    }
}

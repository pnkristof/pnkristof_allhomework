﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pn_kristof_rdi2.RogueGame.Jatek.Jatekter;
using pn_kristof_rdi2.RogueGame.Jatek.Megjelenites;

namespace pn_kristof_rdi2.RogueGame.Jatek.Szabalyok
{
    class Jatekos : MozgoJatekElem, IKirajzolhato, IMegjelenitheto
    {
        private string nev;
        private int eletero;
        private int pontszam;

        public event JatekosValtozasKezelo JatekosValtozas;

        public string Nev
        {
            get
            {
                return nev;
            }
        }
        public override double Meret
        {
            get
            {
                return 0.2;
            }
        }
        public virtual char Alak
        {
            get
            {
                return Aktiv ? '\u263A' : '\u263B';
            }
        }
        public int[] MegjelenitendoMeret
        {
            get
            {
                return ter.MegjelenitendoMeret;
            }
        }

        public Jatekos(string nev, int x, int y, JatekTer ter) : base(x, y, ter)
        {
            this.nev = nev;
            eletero = 100;
            pontszam = 0;
        }

        public override void Utkozes(JatekElem elem)
        { }
        public void Serul(int sebzes)
        {
            if (sebzes != 0 && this.Aktiv)
            {
                JatekosValtozas?.Invoke(this, pontszam, eletero -= eletero - sebzes > 0 ? sebzes : 0);
            }
            if (eletero == 0)
            {
                Aktiv = false;
            }
        }
        public void PontSzerez(int pont)
        {
            if (pont != 0)
            {
                JatekosValtozas?.Invoke(this, pontszam += pont, eletero);
            }
        }
        public void Megy(int rx, int ry)
        {
            AtHelyez(X + rx, Y + ry);
        }
        public IKirajzolhato[] MegjelenitendoElemek()
        {
            var latott = ter.MegadottHelyenLevok(X, Y, 5);
            int count = 0;
            foreach (var elem in latott)
                if (elem is IKirajzolhato)
                {
                    count++;
                }
            var kirajzolhato = new IKirajzolhato[count];

            foreach (var elem in latott)
            {
                kirajzolhato[--count] = elem as IKirajzolhato;
            }

            return kirajzolhato;
        }
    }
}

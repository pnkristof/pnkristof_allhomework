﻿using pn_kristof_rdi2.RogueGame.Jatek.Jatekter;
using pn_kristof_rdi2.RogueGame.Jatek.Megjelenites;
using pn_kristof_rdi2.RogueGame.Jatek.Szabalyok;
using RogueGame.Jatek.Automatizmus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pn_kristof_rdi2.RogueGame.Jatek.Keret
{
    
    class Keret
    {
        Random rnd = new Random();

        const int PALYA_MERET_X = 21;
        const int PALYA_MERET_Y = 11;
        const int KINCSEK_SZAMA = 10;

        private bool jatekVege = false;
        private int megtalaltKincsek;

        private Jatekter.JatekTer ter;

        private OrajelGenerator generator;

        public Keret()
        {
            ter = new JatekTer(PALYA_MERET_X, PALYA_MERET_Y);
            jatekVege = false;
            generator = new OrajelGenerator();
            PalyaGeneralas();
        }


        private void PalyaGeneralas()
        {
            Random rnd = new Random();
            for (int i = 0; i < PALYA_MERET_X; i++)
            {
                new Fal(i, 0, ter);
                new Fal(i, PALYA_MERET_Y - 1, ter);
                if (i > 0 && i < PALYA_MERET_Y - 1)
                {
                    new Fal(0, i, ter);
                    new Fal(PALYA_MERET_X - 1, i, ter);
                }
            }
            for (int i = 0; i < KINCSEK_SZAMA;)
            {
                int x = rnd.Next(1, PALYA_MERET_X - 1), y = rnd.Next(1, PALYA_MERET_Y - 1);
                if (ter.MegadottHelyenLevok(x, y).Length == 0 && !(x == y && y == 1))
                {
                    (new Kincs(x, y, ter)).KincsFelvetel += KincsFelvetelTortent;
                    i++;
                }
            }
        }
    

        public void Futtatas()
        {
            var jatekos = new Jatekos("Béla", 1, 1, ter);
            var gepiJatekos = new GepiJatekos("Kati", 2, 2, ter);
            var gonoszGepiJatekos = new GonoszGepiJatekos("Laci", 3, 3, ter);
            var megjelenito2 = new KonzolosMegjelenito(25, 0, jatekos);
            var megjelenito = new KonzolosMegjelenito(0, 0, ter);
            var eredmenyAblak = new KonzolosEredmenyAblak(0, 12, 5);

            jatekos.JatekosValtozas += JatekosValtozasTortent;

            eredmenyAblak.JatekosFeliratkozas(jatekos);

            generator.Felvetel(gepiJatekos);
            generator.Felvetel(gonoszGepiJatekos);
            generator.Felvetel(megjelenito);
            generator.Felvetel(megjelenito2);

            do
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                switch (key.Key)
                {
                    case ConsoleKey.LeftArrow:
                        jatekos.Megy(-1, 0);
                        break;
                    case ConsoleKey.RightArrow:
                        jatekos.Megy(1, 0);
                        break;
                    case ConsoleKey.UpArrow:
                        jatekos.Megy(0, -1);
                        break;
                    case ConsoleKey.DownArrow:
                        jatekos.Megy(0, 1);
                        break;
                    case ConsoleKey.Escape:
                        jatekVege = true;
                        break;
                    default:
                        break;
                }
            } while (!jatekVege);
        }

        private void KincsFelvetelTortent(Kincs kincs, Jatekos jatekos)
        {
            jatekos.PontSzerez(10);
            if (++megtalaltKincsek == KINCSEK_SZAMA)
                jatekVege = true;
        }
        private void JatekosValtozasTortent(Jatekos jatekos, int ujPont, int ujElet)
        {
            if (ujElet == 0)
                jatekVege = true;
        }
    }
}

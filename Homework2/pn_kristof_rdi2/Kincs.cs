﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pn_kristof_rdi2.RogueGame.Jatek.Jatekter;
using pn_kristof_rdi2.RogueGame.Jatek.Megjelenites;

namespace pn_kristof_rdi2.RogueGame.Jatek.Szabalyok
{
    class Kincs : RogzitettJatekElem, IKirajzolhato
    {
        public event KincsFelvetelKezelo KincsFelvetel;

        public override double Meret
        {
            get { return 1; }
        }
        public char Alak
        {
            get { return '\u2666'; }
        }

        public Kincs(int x, int y, JatekTer ter) : base(x, y, ter)
        { }

        public override void Utkozes(JatekElem elem)
        {
            if (elem is Jatekos)
            {
                KincsFelvetel?.Invoke(this, elem as Jatekos);
                ter.Torles(this);
            }
        }
    }
}

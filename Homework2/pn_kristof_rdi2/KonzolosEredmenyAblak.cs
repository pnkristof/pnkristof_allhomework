﻿using pn_kristof_rdi2.RogueGame.Jatek.Szabalyok;
using RogueGame.Jatek.Megjelenites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pn_kristof_rdi2.RogueGame.Jatek.Megjelenites
{
    class KonzolosEredmenyAblak
    {
        private int pozX;
        private int pozY;
        private int maxSorSzam;
        private int sor;

        public KonzolosEredmenyAblak(int pozX, int pozY, int maxSorSzam)
        {
            this.pozX = pozX;
            this.pozY = pozY;
            this.maxSorSzam = maxSorSzam;
        }

        private void JatekosValtozasTortent(Jatekos jatekos, int ujPont, int ujElet)
        {
            SzalbiztosKonzol.KiirasXY(pozX, pozY + sor, string.Format("Játékos neve: {0}, pontszáma: {1}, életereje: {2}", jatekos.Nev, ujPont, ujElet));
            sor = (sor + 1) % maxSorSzam;
        }

        public void JatekosFeliratkozas(Jatekos jatekos)
        {
            jatekos.JatekosValtozas += JatekosValtozasTortent;
        }
    }
}

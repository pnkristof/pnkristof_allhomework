﻿using pn_kristof_rdi2.RogueGame.Jatek.Automatizmus;
using RogueGame.Jatek.Megjelenites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pn_kristof_rdi2.RogueGame.Jatek.Megjelenites
{
    class KonzolosMegjelenito : IAutomatikusanMukodo
    {

        private IMegjelenitheto forras;
        private int pozX, pozY;

        public int MukodesIntervallum
        {
            get { return 1; }
        }

        public KonzolosMegjelenito(int x, int y, IMegjelenitheto forras)
        {
            this.forras = forras;
            this.pozX = x;
            this.pozY = y;
        }

        public void Megjelenites()
        {
            var meret = forras.MegjelenitendoMeret; 
            var megjelenitendok = forras.MegjelenitendoElemek();
            for (int i = 0; i < meret[0]; i++)
            {
                for (int j = 0; j < meret[1]; j++)
                {
                    SzalbiztosKonzol.KiirasXY(pozX + i, pozY + j, ' ');
                    foreach (var elem in megjelenitendok)
                    {
                        if (elem.X == i && elem.Y == j)
                        {
                            SzalbiztosKonzol.KiirasXY(pozX + i, pozY + j, elem.Alak);
                        }
                    }
                }
            }
        }

        public void Mukodik()
        {
            Megjelenites();
        }
    }
}

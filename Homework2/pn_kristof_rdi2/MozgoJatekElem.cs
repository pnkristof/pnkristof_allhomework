﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pn_kristof_rdi2.RogueGame.Jatek.Jatekter
{
    abstract class MozgoJatekElem : JatekElem
    {
        private bool aktiv;
        public bool Aktiv { get { return aktiv; } set { aktiv = value; } }

        public MozgoJatekElem(int x, int y, JatekTer ter) : base(x,y,ter)
        {
            aktiv = true;
        }

        public void AtHelyez(int ujx, int ujy)
        {
            var ujHelyenLevok = ter.MegadottHelyenLevok(ujx, ujy);
            foreach (var elem in ujHelyenLevok)
            {
                if (!this.aktiv)
                    return;
                this.Utkozes(elem);
                elem.Utkozes(this);
            }
            ujHelyenLevok = ter.MegadottHelyenLevok(ujx, ujy);
            double ujHelyenHely = 1;
            foreach (var elem in ujHelyenLevok)
                ujHelyenHely -= elem.Meret;
            if (Meret <= ujHelyenHely)
            {
                X = ujx;
                Y = ujy;
            }
        }

    }
}

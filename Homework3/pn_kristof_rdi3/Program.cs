﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace hw3_rdi
{
    class Program
    {

        //RDI képzés 3. heti házi feladat
        //minden függvény és osztály ebben a fájlban található
        static void Main(string[] args)
        {
            XDocument xd = XDocument.Load("http://www.szabozs.hu/_DEBRECEN/kerteszg/mondial-3.0.xml");

            var Countries = GetData.GetCountries(xd);

            //1. feladat
            //összes népsűrűség csökkenő sorrendben
            var PopDensity = from x in Countries
                             orderby x.TotalPopulation / x.Area descending
                             select new
                             {
                                 Country = x,
                                 Density = x.TotalPopulation / x.Area
                                 //népsűrűség = népesség/terület (fő/km^2)
                             };

            //foreach (var item in PopDensity)
            //{
            //    Console.WriteLine(item);
            //}

            Console.WriteLine("1. Feladat\n" +
                               "Legnagyobb népsűrűségű ország: {0} ({1} fő/km^2)",
                               PopDensity.First().Country.Name,
                               PopDensity.First().Density);


            Console.WriteLine();
            //2. feladat
            //demokratikus országok kiválasztása
            Regex demRgx = new Regex("democra(tic|cy)");
            var DemocraticCountries = from x in Countries
                                      where demRgx.IsMatch(x.Government) //a government mező tartalmazza-e a democratic/democracy kifejezést
                                      select x;


            Console.WriteLine("2. Feladat\n" +
                              "Az országok {0}%-ában valamilyen formájú demokrácia van."
                              , Math.Round(100 * (double)DemocraticCountries.Count() / Countries.Count(), 2));
            //demokratikus országok száma/összes ország (2 tizedes jegyre kerekítve)

            Console.WriteLine("Országok száma: " + Countries.Count() + " Demokrácia: " + DemocraticCountries.Count());


            Console.WriteLine();
            //3. feladat
            var CountOfGroups = (from x in Countries
                                  orderby x.Religions.Count() + x.Ethnics.Count() descending
                                  select new
                                  {
                                      Name = x.Name,
                                      Groups = x.Religions.Count() + x.Ethnics.Count()
                                  }).Take(5); //csak az első 5 a példa kedvéért

            Console.WriteLine("3. Feladat\n" +
                              "Országok etnikai és vallási csoportok összegének száma alapján rendezve:");

            foreach (var item in CountOfGroups)
            {
                Console.WriteLine("{0}: {1}", item.Name, item.Groups);
            }




            //foreach (var item in countries.First().Religions)
            //{
            //    Console.WriteLine(item.Name + " " + item.Percentage);
            //}



            Console.WriteLine();
            //4. Feladat
            var BordersLength = (from x in Countries
                                 select new
                                 {
                                     Name = x.Name,
                                     BorderLength = x.Borders.Sum(y => y.Length)
                                 }).Take(5);//csak az első 5 a példa kedvéért

            Console.WriteLine("4. Feladat\n" +
                              "Országok határhosszai:");

            foreach (var item in BordersLength)
            {
                Console.WriteLine("{0}: {1}", item.Name, item.BorderLength);
            }


            Console.WriteLine();
            //5. feladat
            //folyók helyek száma alapján csökkenő sorrendben
            var Rivers = GetData.GetRivers(xd);
            var RiversOrderedByLocation = (from x in Rivers
                                           orderby x.Locations.Count() descending
                                           select x)
                                          .Take(5); //az első 5





            Console.WriteLine("5.Feladat\n" +
                              "Az öt legtöbb országot metsző folyó:");

            foreach (var river in RiversOrderedByLocation)
            {
                Console.WriteLine("{0}:\t{1}", river.Name, river.Locations.Count());
            }


            Console.WriteLine();
            //6. feladat
            //var Waters = GetData.GetWaters(xd); összes víz


            //vizek helyei az országokban mennyiség szerint csökkenő sorrendben 
            //-  minden víz csak egyszer tartalmazza egy ország ID-jét,
            //így például a Donau, ami egyébként 19 'located' elemet tartalmaz, valójában csak két különbözőt
            var WaterLocsOrderedByCount = (from x in GetData.GetWaters(xd).SelectMany(y => y.Locations)
                                           group x by x into g
                                           orderby g.Count() descending
                                           select new
                                           {
                                               Name = Countries.Where(y => y.ID == g.Key).First().Name,
                                               ID = g.Key,
                                               Count = g.Count()
                                           }).Take(5); //az első 5



            Console.WriteLine("6. Feladat\n" +
                              "Vizek előfordulása az országokban:");

            foreach (var location in WaterLocsOrderedByCount)
            {
                Console.WriteLine("{0}:\t{1}", location.Name, location.Count);
            }



            Console.WriteLine();
            //7. feladat
            //folyók, amelyek tengerbe torkollanak -> azon folyók kiválasztása, amelyek ToID-ja megtalálható a tengerek ID-ja között
            //var RiversToSea = from x in rivers
            //                  where
            //                  (
            //                      from y in GetData.GetSeas(xd)
            //                      select y.ID
            //                  ).Contains(x.ToID)
            //                  select x;


            var RiversToSea = (from x in Rivers
                               where
                               (
                                   from y in GetData.GetSeas(xd)
                                   select y.ID
                               ).Contains(x.ToID)
                               group x by x.ToID into g
                               orderby g.Count() descending
                               select new
                               {
                                   Name = GetData.GetSeas(xd).Where(y => y.ID == g.Key).First().Name,
                                   Count = g.Count()
                               }).Take(5); //az első 5

            Console.WriteLine("7. Feladat\n" +
                              "Tengerek, amelyekbe a legtöbb folyó torkollik:");
            foreach (var river in RiversToSea)
            {
                Console.WriteLine("{0}:\t{1}", river.Name, river.Count);
            }


            Console.WriteLine();
            //8. feladat
            //az összes etnikum
            //var Ethnics = countries.SelectMany(x => x.Ethnics);
            //összegezve, csökkenő sorrendben
            var SumEthnics = (from x in Countries.SelectMany(x => x.Ethnics)
                              group x by x.Name into g
                              orderby g.Sum(y => y.Quantity) descending
                              select new
                              {
                                  Name = g.Key,
                                  Sum = g.Sum(x => x.Quantity)
                              }).Take(5);

            Console.WriteLine("8. Feladat\n" +
                              "A világ etnikumai:");
            foreach (var item in SumEthnics)
            {
                Console.WriteLine("{0}:\t{1:N}", item.Name, item.Sum);
            }

            Console.ReadLine();
        }



    }

    class Country
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string CapitalID { get; set; }
        public int TotalPopulation { get; set; }
        public int Area { get; set; }
        public string Government { get; set; }
        public IEnumerable<Group> Religions { get; set; }
        public IEnumerable<Group> Ethnics { get; set; }
        public IEnumerable<Border> Borders { get; set; }
    }


    class Water
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public IEnumerable<string> Locations { get; set; }
    }

    class River : Water
    {
        public string ToID { get; set; }
        //public int Length { get; set; }
    }



    enum GroupType { Ethnical, Religious }
    class Group
    {
        GroupType Type { get; set; }
        public string Name { get; set; }
        public double Percentage { get; set; }
        public int Quantity { get; set; }

        public Group(GroupType type, string name, double percentage, int quantity)
        {
            Type = type;
            Name = name;
            Percentage = percentage;
            Quantity = quantity;
        }
    }

    class Border
    {
        public string ID1 { get; set; }
        public string ID2 { get; set; }
        public double Length { get; set; }

        public Border(string id1, string id2, double length)
        {
            ID1 = id1;
            ID2 = id2;
            Length = length;
        }
    }


    class GetData
    {


        public static IEnumerable<Country> GetCountries(XDocument xd)
        {
            var countries = from x in xd.Descendants("country")
                            select new Country
                            {
                                Name = x.Attribute("name").Value,
                                ID = x.Attribute("id").Value,
                                CapitalID = x.Attribute("capital").Value,
                                TotalPopulation = int.Parse(x.Attribute("population").Value),
                                Area = int.Parse(x.Attribute("total_area").Value),
                                Government = x.Attribute("government").Value,

                                Religions = from y in x.Elements("religions")
                                            select new Group(GroupType.Religious, y.Value, double.Parse(y.Attribute("percentage").Value.Replace('.', ',')),//típus, név, százalék
                                            (int)(int.Parse(x.Attribute("population").Value) * double.Parse(y.Attribute("percentage").Value.Replace('.', ',')) / 100)),//mennyiség

                                Ethnics = from y in x.Elements("ethnicgroups")
                                          select new Group(GroupType.Ethnical, y.Value, double.Parse(y.Attribute("percentage").Value.Replace('.', ',')),//típus, név, százalék
                                          (int)(int.Parse(x.Attribute("population").Value) * double.Parse(y.Attribute("percentage").Value.Replace('.', ',')) / 100)),//mennyiség

                                Borders = from y in x.Elements("border")
                                          select new Border(x.Attribute("id").Value, y.Attribute("country").Value, double.Parse(y.Attribute("length").Value.Replace('.', ',')))//ország 1, ország 2, hossz
                                          //a feladat megoldása szempontjából szükségtelenek az ID adattagok, egy double-öket tartalmazó kollekció is megfelelő lenne

                            };

            return countries;

        }

        public static IEnumerable<River> GetRivers(XDocument xd)
        {

            var rivers = from x in xd.Descendants("river")
                         select new River
                         {
                             Name = x.Attribute("name").Value,
                             ID = x.Attribute("id").Value,
                             //Length = int.Parse(x.Attribute("length").Value),    feladatok megoldásához nem szükséges
                             Locations = (from y in x.Elements("located")
                                          select y.Attribute("country").Value).Distinct(),
                             ToID = x.Element("to").Attribute("water").Value
                         };
            return rivers;


        }

        public static IEnumerable<Water> GetSeas(XDocument xd)
        {
            var seas = from x in xd.Descendants("sea")
                       select new Water
                       {
                           Name = x.Attribute("name").Value,
                           ID = x.Attribute("id").Value,
                           Locations = (from y in x.Elements("located")
                                        select y.Attribute("country").Value).Distinct()
                       };

            return seas;
        }

        public static IEnumerable<Water> GetLakes(XDocument xd)
        {
            var lakes = from x in xd.Descendants("lake")
                        select new Water
                        {
                            Name = x.Attribute("name").Value,
                            ID = x.Attribute("id").Value,
                            Locations = (from y in x.Elements("located")
                                         select y.Attribute("country").Value).Distinct()
                        };

            return lakes;
        }

        public static IEnumerable<Water> GetWaters(XDocument xd)
        {
            var rivers = GetRivers(xd);
            var lakes = GetLakes(xd);
            var seas = GetSeas(xd);

            return rivers.Concat(lakes).Concat(seas);
        }
    }



}

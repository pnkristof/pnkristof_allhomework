﻿using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3_DB_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogic logic = new RealLogic();
            foreach (var item in logic.GetBooks())
            {
                Console.WriteLine(item.TITLE);
            }

            Book b = new Book { TITLE = "Title1", AUTHOR = "Someone", BOOKNO = 1 };
            logic.AddBook(b);


            foreach (var item in logic.GetBooks())
            {
                Console.WriteLine(item.TITLE);
            }

            logic.DelBook((int)b.BOOKNO);

            foreach (var item in logic.GetBooks())
            {
                Console.WriteLine(item.TITLE);
            }
            Console.ReadLine();
        }
    }
}

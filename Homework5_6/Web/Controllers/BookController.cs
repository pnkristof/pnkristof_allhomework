﻿using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.DTO;

namespace Web.Controllers
{
    public class BookController : ApiController
    {
        ILogic logic;
        public BookController()
        {
            logic = new RealLogic();
        }

        // get api/book/all
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<BookDTO> GetAllBooks()
        {
            var list = logic.GetBooks();
            return BookDTO.Mapper.Map<List<Book>, IEnumerable<BookDTO>>(list);
        }

        //get api/book/del/42
        [HttpGet]
        [ActionName("del")]
        public int DelOneBook(int id)
        {
            logic.DelBook(id);
            return 1;
        }

        //post api/book/add
        [HttpPost]
        [ActionName("add")]
        public int AddOneBook(BookDTO b)
        {
            b.Bookno = logic.GetBooks().Max(x => (int)x.BOOKNO) + 1;

            var book = BookDTO.Mapper.Map<BookDTO, Book>(b);
            logic.AddBook(book);
            return b.Bookno;
        }

        //post api/book/mod
        [HttpPost]
        [ActionName("mod")]
        public int ModOneBook(BookDTO b)
        {
            logic.ModifyBook(b.Bookno, b.Title, b.Author);
            return 1;
        }
    }
}
